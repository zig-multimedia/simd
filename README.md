# simd

A Zig package containing inline functions that call out to inline assembly to compensate for the
current unfortunate lack of intrinsics.

## Usage
`build.zig`:
```zig
exe.addImport("simd", b.dependency("simd", .{}).module("simd"));
```

In your source file:
```
const simd = @import("simd");

const a: simd.U8x8 = .{6, 9, 4, 2, 1, 3, 3, 7};
const b: simd.U8x8 = .{6, 1, 4, 8, 1, 3, 5, 7};
const x = simd.sad(U8x8, a, b);
// do stuff with the sum of absolute differences in x
```
