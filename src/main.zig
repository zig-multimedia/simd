// SPDX-License-Identifier: Unlicense
const std = @import("std");
const assert = std.debug.assert;

// zig fmt: off
pub const U8x8   = @Vector(8,   u8); //  64 bits
pub const U8x16  = @Vector(16,  u8); // 128 bits
pub const U8x32  = @Vector(32,  u8); // 256 bits
pub const U8x64  = @Vector(64,  u8); // 512 bits
pub const U16x4  = @Vector(4,  u16); //  64 bits
pub const U16x8  = @Vector(8,  u16); // 128 bits
pub const U16x16 = @Vector(16, u16); // 256 bits
pub const U16x32 = @Vector(32, u16); // 512 bits
pub const I16x4  = @Vector(4,  i16); //  64 bits
pub const I16x8  = @Vector(8,  i16); // 128 bits
pub const I16x16 = @Vector(16, i16); // 256 bits
pub const I16x32 = @Vector(32, i16); // 512 bits
pub const I32x2  = @Vector(2,  i32); //  64 bits
pub const I32x4  = @Vector(4,  i32); // 128 bits
pub const I32x8  = @Vector(8,  i32); // 256 bits
pub const I32x16 = @Vector(16, i32); // 512 bits
// zig fmt: on

pub const cur_features = Features.get(@import("builtin").cpu);

pub const Features = packed struct {
    have_mmx: bool = false,
    have_sse: bool = false,
    have_sse2: bool = false,
    have_ssse3: bool = false,
    have_avx: bool = false,
    have_avx2: bool = false,
    have_avx512vl: bool = false,
    have_avx512dq: bool = false,
    have_avx512bw: bool = false,
    have_avx512f: bool = false,

    have_neon: bool = false,
    have_neon_fp: bool = false,
    have_neon_fp_movs: bool = false,

    have_rvv_1p0: bool = false,
    have_rvv_32: bool = false,
    have_rvv_64: bool = false,
    have_rvv_128: bool = false,
    have_rvv_256: bool = false,
    have_rvv_512: bool = false,

    have_hard_f32: bool = false,
    have_hard_f64: bool = false,
    have_hard_mul: bool = false,

    pub fn get(cpu: std.Target.Cpu) Features {
        // zig fmt: off
        return if (cpu.arch.isX86()) .{
            .have_mmx      = std.Target.x86.featureSetHas(cpu.features, .mmx),
            .have_sse      = std.Target.x86.featureSetHas(cpu.features, .sse),
            .have_sse2     = std.Target.x86.featureSetHas(cpu.features, .sse2),
            .have_ssse3    = std.Target.x86.featureSetHas(cpu.features, .ssse3),
            .have_avx      = std.Target.x86.featureSetHas(cpu.features, .avx),
            .have_avx2     = std.Target.x86.featureSetHas(cpu.features, .avx2),
            .have_avx512vl = std.Target.x86.featureSetHas(cpu.features, .avx512vl),
            .have_avx512dq = std.Target.x86.featureSetHas(cpu.features, .avx512dq),
            .have_avx512bw = std.Target.x86.featureSetHas(cpu.features, .avx512bw),
            .have_avx512f  = std.Target.x86.featureSetHas(cpu.features, .avx512f),
            .have_hard_mul = true,
        } else if (cpu.arch.isArmOrThumb()) .{
            .have_neon         = std.Target.arm.featureSetHas(cpu.features, .neon),
            .have_neon_fp      = std.Target.arm.featureSetHas(cpu.features, .neonfp),
            .have_neon_fp_movs = std.Target.arm.featureSetHas(cpu.features, .neon_fpmovs),
            .have_hard_mul     = std.Target.arm.featureSetHas(cpu.features, .mul),
        } else if (cpu.arch.isRISCV()) .{
            .have_rvv_32   = std.Target.riscv.featureSetHas(cpu.features, .zvl32b),
            .have_rvv_64   = std.Target.riscv.featureSetHas(cpu.features, .zvl64b),
            .have_rvv_128  = std.Target.riscv.featureSetHas(cpu.features, .zvl128b),
            .have_rvv_256  = std.Target.riscv.featureSetHas(cpu.features, .zvl256b),
            .have_rvv_512  = std.Target.riscv.featureSetHas(cpu.features, .zvl512b),
            .have_hard_f32 = std.Target.riscv.featureSetHas(cpu.features, .f),
            .have_hard_f64 = std.Target.riscv.featureSetHas(cpu.features, .d),
            .have_hard_mul = std.Target.riscv.featureSetHas(cpu.features, .m),
        } else @compileError("unsupported arch");
        // zig fmt: on
    }

    pub fn largestNativeVectorBits(f: Features) ?usize {
        return if (f.have_rvv_512 or f.have_avx512vl)
            512
        else if (f.have_avx2 or f.have_rvv_256)
            256
        else if (f.have_sse2 or f.have_rvv_128)
            128
        else if (f.have_mmx or f.have_rvv_64)
            64
        else if (f.have_rvv_32)
            32
        else
            null;
    }

    pub fn smallestNativeVectorBits(f: Features) ?usize {
        return if (f.have_rvv_32)
            32
        else if (f.have_mmx or f.have_rvv_64)
            64
        else if (f.have_sse2 or f.have_rvv_128)
            128
        else if (f.have_avx2 or f.have_rvv_256)
            256
        else if (f.have_rvv_512 or f.have_avx512vl)
            512
        else
            null;
    }

    pub fn largestNativeVectorLength(f: Features, Child: type) ?usize {
        const bits = f.largestNativeVectorBits() orelse return null;
        return @divFloor(bits, @bitSizeOf(Child));
    }

    pub fn smallestNativeVectorLength(f: Features, Child: type) ?usize {
        const bits = f.smallestNativeVectorBits() orelse return null;
        return @divFloor(bits, @bitSizeOf(Child));
    }

    pub fn supportsVectorBits(f: Features, bits: usize) bool {
        return switch (bits) {
            8 => true,
            16 => true,
            32 => f.have_rvv_32,
            64 => f.have_mmx or f.have_rvv_64,
            128 => f.have_sse2 or f.have_rvv_128,
            256 => f.have_avx2 or f.have_rvv_256,
            512 => f.have_avx512vl or f.have_rvv_512,
            else => false,
        };
    }

    pub fn supportsVectorType(f: Features, comptime T: type) bool {
        return supportsVectorBits(@bitSizeOf(T)) and switch (@typeInfo(T).vector.child) {
            u16, i16, u32, i32, u64, i64 => f.have_mmx or f.have_avx or f.have_avx2,
            bool => true,
            else => false,
        };
    }
};

pub fn WideInt(comptime T: type) type {
    const ti = @typeInfo(T).int;
    return @Type(.{ .int = .{ .signedness = ti.signedness, .bits = ti.bits * 2 } });
}

pub fn VChild(comptime V: type) type {
    const ti = @typeInfo(V).vector;
    return ti.child;
}

pub fn signed(comptime T: type) type {
    const ti = @typeInfo(T).int;
    return @Type(.{ .int = .{ .signedness = .signed, .bits = ti.bits } });
}

pub fn unsigned(comptime T: type) type {
    const ti = @typeInfo(T).int;
    return @Type(.{ .int = .{ .signedness = .signed, .bits = ti.bits } });
}

pub fn vlen(comptime V: type) comptime_int {
    const ti = @typeInfo(V).vector;
    return ti.len;
}

pub inline fn sadEmulated(comptime T: type, lhs: T, rhs: T) WideInt(VChild(T)) {
    const d_lhs: @Vector(vlen(T), signed(WideInt(VChild(T)))) = lhs;
    const d_rhs: @Vector(vlen(T), signed(WideInt(VChild(T)))) = rhs;
    return @reduce(.Add, @abs(d_lhs - d_rhs));
}

/// Computes the sum of absolute differences between `lhs` and `rhs`.
///
/// Supported values for `T`: `U8x8`, `U8x16`, `U8x32`, `U8x64`.
pub inline fn sad(comptime T: type, lhs: T, rhs: T) WideInt(VChild(T)) {
    const v = switch (T) {
        U8x8 => sad_u8x8(lhs, rhs),
        U8x16 => sad_u8x16(lhs, rhs),
        U8x32 => sad_u8x32(lhs, rhs),
        U8x64 => sad_u8x64(lhs, rhs),
        else => @compileError("non-native operation"),
    };

    const res = switch (T) {
        U8x8 => v[0],
        U8x16 => v[0] + v[4],
        U8x32 => v[0] + v[4] + v[8] + v[12],
        U8x64 => v[0] + v[4] + v[8] + v[12] + v[16] + v[20] + v[24] + v[32],
        else => unreachable,
    };

    assert(res == sadEmulated(T, lhs, rhs));

    return res;
}

/// Add horizontally adjacent elements in `lhs` and `rhs`, `result[0] = lhs[0] +% lhs[1]` and so
/// forth.
///
/// Supported values for `T`: `I16x4`, `I16x8`, `I16x16`
pub inline fn hadd(comptime T: type, lhs: T, rhs: T) T {
    return switch (T) {
        I16x4 => hadd_i16x4(lhs, rhs),
        I16x8 => hadd_i16x8(lhs, rhs),
        I16x16 => hadd_i16x16(lhs, rhs),
        else => @compileError("non-native operation"),
    };
}

/// Add horizontally adjacent elements in `lhs` and `rhs` with saturation, `result[0] = lhs[0] +|
/// lhs[1]` and so forth.
///
/// Supported values for `T`: `I16x4`, `I16x8`, `I16x16`
pub inline fn hadds(comptime T: type, lhs: T, rhs: T) T {
    return switch (T) {
        I16x4 => hadds_i16x4(lhs, rhs),
        I16x8 => hadds_i16x8(lhs, rhs),
        I16x16 => hadds_i16x16(lhs, rhs),
        else => @compileError("non-native operation"),
    };
}

/// Subtract horizontally adjacent elements in `lhs` and `rhs`,
/// `result[0] = lhs[0] -% lhs[1]` and so forth.
///
/// Supported values for `T`: `I16x4`, `I16x8`, `I16x16`
pub inline fn hsub(comptime T: type, lhs: T, rhs: T) T {
    return switch (T) {
        I16x4 => hsub_i16x4(lhs, rhs),
        I16x8 => hsub_i16x8(lhs, rhs),
        I16x16 => hsub_i16x16(lhs, rhs),
        else => @compileError("non-native operation"),
    };
}

/// Subtract horizontally adjacent elements in `lhs` and `rhs` with
/// saturation, `result[0] = lhs[0] -| lhs[1]` and so forth.
///
/// Supported values for `T`: `I16x4`, `I16x8`, `I16x16`
pub inline fn hsubs(comptime T: type, lhs: T, rhs: T) T {
    return switch (T) {
        I16x4 => hsubs_i16x4(lhs, rhs),
        I16x8 => hsubs_i16x8(lhs, rhs),
        I16x16 => hsubs_i16x16(lhs, rhs),
        else => @compileError("non-native operation"),
    };
}

/// Multiply every element of `lhs` with every element of `rhs`, then add adjacent elements.
///
/// Roughly does this for I16x4:
/// ```
/// const tmp = @as(I32x4, lhs) *% @as(I32x4, rhs);
/// return @as(I32x2, @truncate(.{tmp[0] +% tmp[1], tmp[2] +% tmp[3]}))
/// ```
///
/// Supported values for `T`: `I16x4`, `I16x8`, `I16x16`, `I16x32`
pub inline fn madd(comptime T: type, lhs: T, rhs: T) @Vector(vlen(T) / 2, WideInt(VChild(T))) {
    return switch (T) {
        I16x4 => madd_i16x4(lhs, rhs),
        I16x8 => madd_i16x8(lhs, rhs),
        I16x16 => madd_i16x16(lhs, rhs),
        I16x32 => madd_i16x32(lhs, rhs),
        else => @compileError("non-native operation"),
    };
}

// asm wrappers start here

pub const have_sad_u8x8 = cur_features.have_sse;
pub const have_sad_u8x16 = cur_features.have_avx or cur_features.have_sse2;
pub const have_sad_u8x32 = cur_features.have_avx2;
pub const have_sad_u8x64 = cur_features.have_avx512bw and cur_features.have_avx512vl;

pub const have_hadds_i16x4 = cur_features.have_ssse3;
pub const have_hadds_i16x8 = cur_features.have_ssse3 or cur_features.have_avx;
pub const have_hadds_i16x16 = cur_features.have_avx2;

pub const have_hadd_i16x4 = cur_features.have_ssse3;
pub const have_hadd_i16x8 = cur_features.have_ssse3 or cur_features.have_avx;
pub const have_hadd_i16x16 = cur_features.have_avx2;

pub const have_hsub_i16x4 = cur_features.have_ssse3;
pub const have_hsub_i16x8 = cur_features.have_ssse3 or cur_features.have_avx;
pub const have_hsub_i16x16 = cur_features.have_avx2;

pub const have_hsubs_i16x4 = cur_features.have_ssse3;
pub const have_hsubs_i16x8 = cur_features.have_ssse3 or cur_features.have_avx;
pub const have_hsubs_i16x16 = cur_features.have_avx2;

pub const have_madd_i16x4 = cur_features.have_mmx;
pub const have_madd_i16x8 = cur_features.have_sse2 or cur_features.have_avx;
pub const have_madd_i16x16 = cur_features.have_avx2;
pub const have_madd_i16x32 = cur_features.have_avx512bw;

pub inline fn madd_i16x4(lhs: I16x4, rhs: I16x4) I32x2 {
    if (!have_madd_i16x4) @compileError("unsupported operation");

    return asm volatile ("pmaddwd %[in], %[out]"
        : [out] "=y" (-> I32x2),
        : [_] "0" (lhs),
          [in] "y" (rhs),
    );
}

pub inline fn madd_i16x8(lhs: I16x8, rhs: I16x8) I32x4 {
    if (!have_madd_i16x8) @compileError("unsupported operation");

    if (comptime cur_features.have_avx) {
        return asm volatile ("vpmaddwd %[rhs], %[lhs], %[out]"
            : [out] "=x" (-> I32x4),
            : [lhs] "x" (lhs),
              [rhs] "x" (rhs),
        );
    }
    return asm volatile ("pmaddwd %[in], %[out]"
        : [out] "=x" (-> I32x4),
        : [_] "0" (lhs),
          [in] "x" (rhs),
    );
}

pub inline fn madd_i16x16(lhs: I16x16, rhs: I16x16) I32x8 {
    if (!have_madd_i16x16) @compileError("unsupported operation");

    return asm volatile ("vpmaddwd %[rhs], %[lhs], %[out]"
        : [out] "=x" (-> I32x8),
        : [lhs] "x" (lhs),
          [rhs] "x" (rhs),
    );
}

pub inline fn madd_i16x32(lhs: I16x32, rhs: I16x32) I32x16 {
    if (!have_madd_i16x32) @compileError("unsupported operation");

    return asm volatile ("vpmaddwd %[rhs], %[lhs], %[out]"
        : [out] "=x" (-> I32x16),
        : [lhs] "x" (lhs),
          [rhs] "x" (rhs),
    );
}

pub inline fn sad_u8x8(lhs: U8x8, rhs: U8x8) U16x4 {
    if (!have_sad_u8x8) @compileError("unsupported operation");

    return asm volatile ("psadbw %[in], %[out]"
        : [out] "=y" (-> U16x4),
        : [_] "0" (lhs),
          [in] "y" (rhs),
    );
}

pub inline fn sad_u8x16(lhs: U8x16, rhs: U8x16) U16x8 {
    if (!have_sad_u8x16) @compileError("unsupported operation");

    if (comptime cur_features.have_avx) {
        return asm volatile ("vpsadbw %[rhs], %[lhs], %[out]"
            : [out] "=x" (-> U16x8),
            : [lhs] "x" (lhs),
              [rhs] "x" (rhs),
        );
    }
    return asm volatile ("psadbw %[in], %[out]"
        : [out] "=x" (-> U16x8),
        : [_] "0" (lhs),
          [in] "x" (rhs),
    );
}

pub inline fn sad_u8x32(lhs: U8x32, rhs: U8x32) U16x16 {
    if (!have_sad_u8x32) @compileError("unsupported operation");

    return asm volatile ("vpsadbw %[rhs], %[lhs], %[out]"
        : [out] "=x" (-> U16x16),
        : [lhs] "x" (lhs),
          [rhs] "x" (rhs),
    );
}

pub inline fn sad_u8x64(lhs: U8x64, rhs: U8x64) U16x32 {
    if (!have_sad_u8x64) @compileError("unsupported operation");

    return asm volatile ("vpsadbw %[rhs], %[lhs], %[out]"
        : [out] "=x" (-> U16x32),
        : [lhs] "x" (lhs),
          [rhs] "x" (rhs),
    );
}

pub inline fn hadds_i16x4(lhs: I16x4, rhs: I16x4) I16x4 {
    if (!have_hadds_i16x4) @compileError("unsupported operation");

    return asm volatile ("phaddsw %[in], %[out]"
        : [out] "=y" (-> I16x4),
        : [_] "0" (lhs),
          [in] "y" (rhs),
    );
}

pub inline fn hadds_i16x8(lhs: I16x8, rhs: I16x8) I16x8 {
    if (!have_hadds_i16x8) @compileError("unsupported operation");

    if (comptime cur_features.have_avx) {
        return asm volatile ("vphaddsw %[b], %[a], %[out]"
            : [out] "=x" (-> I16x8),
            : [a] "x" (lhs),
              [b] "x" (rhs),
        );
    }
    asm volatile ("phaddsw %[in], %[out]"
        : [out] "=x" (-> I16x8),
        : [_] "0" (lhs),
          [in] "x" (rhs),
    );
}

pub inline fn hadds_i16x16(lhs: I16x16, rhs: I16x16) I16x16 {
    if (!have_hadds_i16x16) @compileError("unsupported operation");

    return asm volatile ("vphaddsw %[b], %[a], %[out]"
        : [out] "=x" (-> I16x16),
        : [a] "x" (lhs),
          [b] "x" (rhs),
    );
}

pub inline fn hadd_i16x4(lhs: I16x4, rhs: I16x4) I16x4 {
    if (!have_hadd_i16x4) @compileError("unsupported operation");

    return asm volatile ("phaddw %[in], %[out]"
        : [out] "=y" (-> I16x4),
        : [_] "0" (lhs),
          [in] "y" (rhs),
    );
}

pub inline fn hadd_i16x8(lhs: I16x8, rhs: I16x8) I16x8 {
    if (!have_hadd_i16x8) @compileError("unsupported operation");

    if (comptime cur_features.have_avx) {
        return asm volatile ("vphaddw %[b], %[a], %[out]"
            : [out] "=x" (-> I16x8),
            : [a] "x" (lhs),
              [b] "x" (rhs),
        );
    }
    asm volatile ("phaddw %[in], %[out]"
        : [out] "=x" (-> I16x8),
        : [_] "0" (lhs),
          [in] "x" (rhs),
    );
}

pub inline fn hadd_i16x16(lhs: I16x16, rhs: I16x16) I16x16 {
    if (!have_hadd_i16x16) @compileError("unsupported operation");

    return asm volatile ("vphaddw %[b], %[a], %[out]"
        : [out] "=x" (-> I16x16),
        : [a] "x" (lhs),
          [b] "x" (rhs),
    );
}

pub inline fn hsubs_i16x4(lhs: I16x4, rhs: I16x4) I16x4 {
    if (!have_hsubs_i16x4) @compileError("unsupported operation");

    return asm volatile ("phsubsw %[in], %[out]"
        : [out] "=y" (-> I16x4),
        : [_] "0" (lhs),
          [in] "y" (rhs),
    );
}

pub inline fn hsubs_i16x8(lhs: I16x8, rhs: I16x8) I16x8 {
    if (!have_hsubs_i16x8) @compileError("unsupported operation");

    if (comptime cur_features.have_avx) {
        return asm volatile ("vphsubsw %[b], %[a], %[out]"
            : [out] "=x" (-> I16x8),
            : [a] "x" (lhs),
              [b] "x" (rhs),
        );
    }
    asm volatile ("phsubsw %[in], %[out]"
        : [out] "=x" (-> I16x8),
        : [_] "0" (lhs),
          [in] "x" (rhs),
    );
}

pub inline fn hsubs_i16x16(lhs: I16x16, rhs: I16x16) I16x16 {
    if (!have_hsubs_i16x16) @compileError("unsupported operation");

    return asm volatile ("vphsubsw %[b], %[a], %[out]"
        : [out] "=x" (-> I16x16),
        : [a] "x" (lhs),
          [b] "x" (rhs),
    );
}

pub inline fn hsub_i16x4(lhs: I16x4, rhs: I16x4) I16x4 {
    if (!have_hsub_i16x4) @compileError("unsupported operation");

    return asm volatile ("phsubw %[in], %[out]"
        : [out] "=y" (-> I16x4),
        : [_] "0" (lhs),
          [in] "y" (rhs),
    );
}

pub inline fn hsub_i16x8(lhs: I16x8, rhs: I16x8) I16x8 {
    if (!have_hsub_i16x8) @compileError("unsupported operation");

    if (comptime cur_features.have_avx) {
        return asm volatile ("vphsubw %[b], %[a], %[out]"
            : [out] "=x" (-> I16x8),
            : [a] "x" (lhs),
              [b] "x" (rhs),
        );
    }
    asm volatile ("phsubw %[in], %[out]"
        : [out] "=x" (-> I16x8),
        : [_] "0" (lhs),
          [in] "x" (rhs),
    );
}

pub inline fn hsub_i16x16(lhs: I16x16, rhs: I16x16) I16x16 {
    if (!have_hsub_i16x16) @compileError("unsupported operation");

    return asm volatile ("vphsubw %[b], %[a], %[out]"
        : [out] "=x" (-> I16x16),
        : [a] "x" (lhs),
          [b] "x" (rhs),
    );
}
